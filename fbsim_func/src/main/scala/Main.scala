package fbAPI
import akka.actor.{ActorSystem, Props, Actor, ActorRef}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import java.nio.ByteBuffer
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.concurrent.duration.Duration._
import scala.concurrent._
import scala.util.Random
import spray.can.Http
import spray.client.pipelining._
import spray.http._
import spray.http.HttpHeaders.Authorization
import spray.httpx.marshalling._
import spray.json._
import java.security.MessageDigest
import scala.collection.mutable.SynchronizedBuffer

import HttpMethods._
import MyJsonProtocol._

object Boot extends App {

	implicit val system = ActorSystem("fbAPI")
	implicit val timeout = Timeout(5.seconds)

	def createnode(): String = {
		var temp = RSA.generateKeyPair()
		val form: FormData = FormData(Seq(("content", helpers.prepareKeyFile(temp))))
		val future = (IO(Http) ? HttpRequest(POST, Uri("http://localhost:8088/"), List(), marshal(form).right.get))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
		helpers.insertNode(response.entity.asString, temp)
		println(response.entity.asString)
		return response.entity.asString
	}

	def setfriend(uid: String, nodes: Array[String]) = {
		val form: FormData = FormData(Seq(("content", helpers.preparePlainFile(uid, "friendlist", nodes.toJson.prettyPrint))))
		val future = (IO(Http) ? HttpRequest(PUT, Uri("http://localhost:8088/" + uid + "/friendlist"), List(), marshal(form).right.get))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
	}

	def postresource(uid: String, message: String): String = {
		val key = AES.generateKey()
		val form: FormData = FormData(Seq(("content", helpers.prepareResourceFile(uid, message, key))))
		val future = (IO(Http) ? HttpRequest(POST, Uri("http://localhost:8088/"), List(), marshal(form).right.get))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
		helpers.insertResource(response.entity.asString, message, key)
		return response.entity.asString
	}

	def message(uid: String, destination: String, resource: String) = {
		val form: FormData = FormData(Seq(("content", helpers.prepareMessageFile(uid, destination, resource))))
		val future = (IO(Http) ? HttpRequest(PUT, Uri("http://localhost:8088/" + destination), List(), marshal(form).right.get))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
	}

	def getresource(path: String): String = {
		val future = (IO(Http) ? HttpRequest(GET, Uri("http://localhost:8088/" + path)))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
		return response.entity.asString
	}

	def getmessage(uid: String, path: String): String = {
		val message = getresource(path)
		val msgFile = message.parseJson.convertTo[MessageFile]
		val resource = getresource(msgFile.resourcePath)
		val resFile = resource.parseJson.convertTo[SignedFile].content.parseJson.convertTo[ResourceFile]
		val key = RSA.privateDecrypt(msgFile.key, helpers.nodes(uid)._1)
		return new String(AES.decrypt(resFile.content, key))
	}






	val node1 = createnode()
	val grp1 = createnode()
	val node2 = createnode()

	setfriend(grp1, Array(node1))
	setfriend(node2, Array(grp1))
	setfriend(node1, Array(node2))

	val hash = postresource(node1, "TestMessage - Howdy!")

	message(node1, node2 + "/" + grp1 + "/" + node1, node1 + "/" + hash)

	val msg = getmessage(node2, node2 + "/" + grp1 + "/" + node1 + "/" + hash)
	println(msg)
	if(msg == "TestMessage - Howdy!")
		println("end to end test passed")
	else
		println("test failed")

}
