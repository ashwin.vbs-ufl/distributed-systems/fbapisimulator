package fbAPI
import spray.json._
import DefaultJsonProtocol._
import spray.httpx.marshalling._
import spray.can.Http
import spray.client.pipelining._
import spray.http._
import akka.io.IO


import HttpMethods._

// class GetResource(input: ResourceFile, timestamp: Long, key: Array[Byte]){
// 	var modified: Long = timestamp
// 	var author: Array[Byte] = input.author
// 	var content: Array[Byte] = input.content
// }
//
case class PutResource(destination: String, resourcePath: String, key: Array[Byte], timestamp: Long)

case class MessageFile(resourcePath: String, key: Array[Byte], timestamp: Long)

case class SignedMessage(uid: String, content: String, signature: Array[Byte]) //content can be ResrouceFile or KeyFile
case class SignedFile(content: String, signature: Array[Byte]) //content can be ResrouceFile or KeyFile //string SignedFile is returned for get method

case class ResourceFile(content: Array[Byte], selfkey: Array[Byte], timestamp: Long)
case class KeyFile(key: Array[Byte], path: String, timestamp: Long)

case class DeleteMessage(path: String, timestamp: Long)

object MyJsonProtocol extends DefaultJsonProtocol {
	implicit val ResourceFileFormat = jsonFormat3(ResourceFile.apply)
	implicit val KeyFileFormat = jsonFormat3(KeyFile.apply)
	implicit val SignedMessageFormat = jsonFormat3(SignedMessage.apply)
	implicit val SignedFileFormat = jsonFormat2(SignedFile.apply)
	implicit val DeleteMessageFormat = jsonFormat2(DeleteMessage.apply)
	implicit val PutResourceFormat = jsonFormat4(PutResource.apply)
	implicit val MessageFileFormat = jsonFormat3(MessageFile.apply)
}
