Description:
Server application that serves a REST API for a social networking service. Allows for profile pages, activity feeds, file sharing and messaging.
The API has been implemented in a secure, anonymous manner – not even the server can know the contents of messages and files shared in the network.

Files and messages can be encrypted using AES 256 depending on the client implementation. Files are shared using a message to the recipient with the encryption key which itself is encrypted using the recipient's public key. As the plain text files and keys are handled only on the client side and only by the sender or recipient, the service is entirely secure.

Prerequisites:

1. Sbt

2. Scala


Instructions to run the program:

1. sbt build at the project directories.

2. sbt run at the project root will execute the programs.

3. Server has to be executed first followed by either fbsim_func (functional test) or fbsim_perf (performance test)

TODO:
Change the rest request authentication mechanism to improve performance.