package fbAPI
import akka.actor.{ActorSystem, Props, Actor, ActorRef}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import java.nio.ByteBuffer
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration._
import scala.concurrent.duration.Duration._
import scala.concurrent._
import scala.util.Random
import spray.can.Http
import spray.client.pipelining._
import spray.http._
import spray.http.HttpHeaders.Authorization
import spray.httpx.marshalling._
import spray.json._
import java.security.MessageDigest
import scala.collection.mutable.SynchronizedBuffer

import HttpMethods._
import MyJsonProtocol._

class RequestActor extends Actor{
	implicit var system: ActorSystem = null
	var count: Long = 0
	def receive = {
		case ctxt: ActorSystem => {
			system = ctxt
		}
		case request: HttpRequest => {
			IO(Http) ! request
		}
		case response: HttpResponse => {
			count = count + 1
		}

	}

	override def postStop() {
		println(count)
	}
}


object Boot extends App {

	implicit val system = ActorSystem("fbAPI")
	val child = system.actorOf(Props[RequestActor])
	child ! system
	implicit val timeout = Timeout(5.seconds)

	def createnode(): String = {
		var temp = RSA.generateKeyPair()
		val form: FormData = FormData(Seq(("content", helpers.prepareKeyFile(temp))))
		val future = (IO(Http) ? HttpRequest(POST, Uri("http://localhost:8088/"), List(), marshal(form).right.get))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
		helpers.insertNode(response.entity.asString, temp)
		println(response.entity.asString)
		return response.entity.asString
	}

	def setfriend(uid: String, nodes: Array[String]) = {
		val form: FormData = FormData(Seq(("content", helpers.preparePlainFile(uid, "friendlist", nodes.toJson.prettyPrint))))
		val future = (IO(Http) ? HttpRequest(PUT, Uri("http://localhost:8088/" + uid + "/friendlist"), List(), marshal(form).right.get))
		val response: HttpResponse = Await.result(future, timeout.duration).asInstanceOf[HttpResponse]
	}

	def postperf(uid: String, message: String) = {
		val key = AES.generateKey()
		val form: FormData = FormData(Seq(("content", helpers.prepareResourceFile(uid, message, key))))
		child ! HttpRequest(POST, Uri("http://localhost:8088/"), List(), marshal(form).right.get)
	}

	def messageperf(uid: String, destination: String, resource: String) = {
		val form: FormData = FormData(Seq(("content", helpers.prepareMessageFile(uid, destination, resource))))
		child ! HttpRequest(PUT, Uri("http://localhost:8088/" + destination), List(), marshal(form).right.get)
	}

	def getperf(path: String) = {
		child ! HttpRequest(GET, Uri("http://localhost:8088/" + path))
	}


	var nodelist: ArrayBuffer[String] = new ArrayBuffer()

	var i: Int = 0
	for(i <- 0 to 99)
		nodelist += createnode()
	for(i <- 0 to 99)
		setfriend(nodelist(i), nodelist.toArray)
	val starttime = System.currentTimeMillis()
	i = 0
	while((System.currentTimeMillis() - starttime )< 180000){
		i = Random.nextInt(100)
		if(i == 0)
			postperf(nodelist(Random.nextInt(100)), "random")
		else if(i < 5){
			val j = Random.nextInt(100)
			var k = Random.nextInt(100)
			while(j == k) {k = Random.nextInt(100)}
			messageperf(nodelist(j), nodelist(k) + "/" + nodelist(j), nodelist(j) + "/friendlist")
		}
		else{
			getperf(nodelist(Random.nextInt(100)) + "/friendlist")
		}
	}
	system stop child
}
