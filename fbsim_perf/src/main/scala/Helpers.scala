package fbAPI

import java.lang.String
import scala.collection.mutable.{Map, SynchronizedMap, HashMap}
import spray.json._

import MyJsonProtocol._

object helpers{
	var nodes: Map[String, (Array[Byte], Array[Byte])] = null
	var resources: Map[String, (String, Array[Byte])] = null

	val init = {
		nodes = {new HashMap[String, (Array[Byte], Array[Byte])] with SynchronizedMap[String, (Array[Byte], Array[Byte])]}
		resources = {new HashMap[String, (String, Array[Byte])] with SynchronizedMap[String, (String, Array[Byte])]}
	}

	def firstnlast(path: String): (String, String) = {
		var first = ""
		var last = ""
		val pos = path.indexOf("/")
		if(pos == -1)
			return (path, path)
		else
			first = path.substring(0, pos)

		val pos2 = path.lastIndexOf("/")
		last = path.substring(pos2 + 1)
		return (first, last)
	}

	def signMessage(uid: String, message: String): String = {
		return SignedMessage(uid, message, RSA.sign(message.getBytes(), nodes(uid)._1)).toJson.compactPrint
	}

	def signKey(privateKey: Array[Byte], message: String): String = {
		return SignedMessage("", message, RSA.sign(message.getBytes(), privateKey)).toJson.compactPrint
	}

	def insertNode(uid: String, keypair: (Array[Byte], Array[Byte])) = {
		nodes += (uid -> keypair)
	}

	def insertResource(hash: String, resource: String, key: Array[Byte]) = {
		resources += (hash -> (resource, key))
	}

	def prepareResourceFile(uid: String, plaintext: String, key: Array[Byte]): String = {
		val res = ResourceFile(AES.encrypt(plaintext.getBytes(), key), RSA.publicEncrypt(key, nodes(uid)._2), System.currentTimeMillis())
		return signMessage(uid, res.toJson.compactPrint)
	}

	def prepareKeyFile(keypair: (Array[Byte], Array[Byte])): String = {
		val keyFile = KeyFile(keypair._2, "prime", System.currentTimeMillis())
		return signKey(keypair._1, keyFile.toJson.compactPrint)
	}

	def prepareDeleteMessage(uid: String, path: String): String = {
		val msg = DeleteMessage(path, System.currentTimeMillis())
		return signMessage(uid, msg.toJson.compactPrint)
	}

	def prepareMessageFile(uid: String, destination: String, resource: String): String = {
		val msg = PutResource(destination, resource, Array() , System.currentTimeMillis())
		return signMessage(uid, msg.toJson.compactPrint)
	}

	def preparePlainFile(uid: String, fileName: String, resource: String): String = {
		val msg = PutResource(uid + "/" + fileName, resource, "".getBytes() , System.currentTimeMillis())
		return signMessage(uid, msg.toJson.compactPrint)
	}
}
