package fbAPI

import akka.actor.{Props, ActorRef, Actor, Kill}

class ActBEInstance extends Actor{
	var db: DBWrapper = null

	def receive = {
		case initBEInstance(path: String) => db = new FSWrapper(path)
		case GETRequestMsg(path: String) => {
			sender ! GETResultMsg(db.get(path))
		}
		case POSTRequestMsg(uid: String, res: SignedFile) => {
			sender ! POSTResultMsg(db.post(uid, res))
		}
		case DELETERequestMsg(uid: String, res: String) => {
// 			sender ! DELETEResultMsg(resList.map(x => db.delete(uid, x)))
			sender ! DELETEResultMsg(db.delete(uid, res))
		}
		case PUTRequestMsg(uid: String, dest: String, res: MessageFile) => {
// 			var result: Array[Boolean] = resList.map(x => db.put(uid, x))
			sender ! PUTResultMsg(db.put(uid, dest, res))
		}
	}
}
