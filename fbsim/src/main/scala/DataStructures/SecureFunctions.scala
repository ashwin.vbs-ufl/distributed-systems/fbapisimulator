package fbAPI

import javax.crypto._
import javax.crypto.spec._
import java.security._
import java.security.spec._

object RSA{
	val keyFactory: KeyFactory = KeyFactory.getInstance("RSA")

	def sign(data: Array[Byte], privateKey: Array[Byte]): Array[Byte] = {
		val rsa: Signature = Signature.getInstance("SHA256withRSA")

		val privateKeySpec: EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey)
		val privateKey2: PrivateKey = keyFactory.generatePrivate(privateKeySpec)
		rsa.initSign(privateKey2)

		rsa.update(data, 0, data.length)

		return rsa.sign()
	}

	def verifySignature(data: Array[Byte], signature: Array[Byte], publicKey: Array[Byte]): Boolean = {
		val rsa: Signature = Signature.getInstance("SHA256withRSA")

		val publicKeySpec: EncodedKeySpec = new X509EncodedKeySpec(publicKey)
		val publicKey2: PublicKey = keyFactory.generatePublic(publicKeySpec)
		rsa.initVerify(publicKey2)

		rsa.update(data, 0, data.length)

		return rsa.verify(signature)
	}

	def publicEncrypt(data: Array[Byte], publicKey: Array[Byte]): Array[Byte] = {
		val publicKeySpec: EncodedKeySpec = new X509EncodedKeySpec(publicKey)
		val publicKey2: PublicKey = keyFactory.generatePublic(publicKeySpec)

		val cipher: Cipher = Cipher.getInstance("RSA")
		cipher.init(Cipher.ENCRYPT_MODE, publicKey2)
		return cipher.doFinal(data)
	}

	def privateEncrypt(data: Array[Byte], privateKey: Array[Byte]): Array[Byte] = {
		val privateKeySpec: EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey)
		val privateKey2: PrivateKey = keyFactory.generatePrivate(privateKeySpec)

		val cipher: Cipher = Cipher.getInstance("RSA")
		cipher.init(Cipher.ENCRYPT_MODE, privateKey2)
		return cipher.doFinal(data)
	}

	def publicDecrypt(data: Array[Byte], publicKey: Array[Byte]): Array[Byte] = {
		val publicKeySpec: EncodedKeySpec = new X509EncodedKeySpec(publicKey)
		val publicKey2: PublicKey = keyFactory.generatePublic(publicKeySpec)

		val cipher: Cipher = Cipher.getInstance("RSA")
		cipher.init(Cipher.DECRYPT_MODE, publicKey2)
		return cipher.doFinal(data)
	}

	def privateDecrypt(data: Array[Byte], privateKey: Array[Byte]): Array[Byte] = {
		val privateKeySpec: EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey)
		val privateKey2: PrivateKey = keyFactory.generatePrivate(privateKeySpec)

		val cipher: Cipher = Cipher.getInstance("RSA")
		cipher.init(Cipher.DECRYPT_MODE, privateKey2)
		return cipher.doFinal(data)
	}

	def generateKeyPair(): (Array[Byte], Array[Byte]) = {
		val keyGen: KeyPairGenerator = KeyPairGenerator.getInstance("RSA")
		keyGen.initialize(1024)
		val key: KeyPair = keyGen.generateKeyPair()
		return (key.getPrivate().getEncoded(), key.getPublic().getEncoded())
	}
}

object AES{
	var initVector: Array[Byte] = Array()
	var saltstring: String = ""

	val init = {
		saltstring = "salts be soo rad"
		initVector = saltstring.getBytes();
	}

	def encrypt(data: Array[Byte], key: Array[Byte]): Array[Byte] = {
		val secret: SecretKeySpec = new SecretKeySpec(key, 0, 32, "AES")
		val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(initVector))
		return cipher.doFinal(data)
	}

	def decrypt(data: Array[Byte], key: Array[Byte]): Array[Byte] = {
		val secret: SecretKeySpec = new SecretKeySpec(key, 0, 32, "AES")
		val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(initVector))
		return cipher.doFinal(data)
	}

	def generateKey(): Array[Byte] = {
		val aesKey = new Array[Byte](32)
		val sr: SecureRandom = new SecureRandom()
		sr.nextBytes(aesKey)
		return aesKey
	}
}

object MD5{
	def hashString(input: String): String = {
		return MD5.hash(input.getBytes()).map("%02x".format(_)).mkString
	}

	def hash(input: Array[Byte]): Array[Byte] = {
		val md = MessageDigest.getInstance("MD5")
		md.update(input)
		return md.digest()
	}
}
