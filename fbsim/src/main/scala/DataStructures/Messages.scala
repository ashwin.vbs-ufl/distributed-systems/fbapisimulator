package fbAPI

import akka.actor.ActorRef
import spray.http.StatusCode

case class initListener(backend: ActorRef)

case class initConnection(canConnection: ActorRef, backend: ActorRef)

case class initBackEnd(count: Int)
case class initBEInstance(root: String)

case class authenticate(userID: Array[Byte], timeHash: Array[Byte])




// getresource
// nodeid/uid/reskey
// returns array of resource and keys
case class GETRequestMsg(path: String)
case class GETResultMsg(result: Option[String])

// createresource
// uid, authkey, {nodeid, resource, ownkey} //if ownkey is empty, create key in public. returns resid
case class POSTRequestMsg(uid: String, res: SignedFile)
case class POSTResultMsg(result: (StatusCode, String))

// deleteresource
// uid, authkey, nodeid/resource //if uid is owner, delete resource
case class DELETERequestMsg(uid: String, res: String)
case class DELETEResultMsg(result: Option[Boolean])

// createresource
// uid, authkey, {nodeid, resource, ownkey} //if ownkey is empty, create key in public. returns resid
case class PUTRequestMsg(uid: String, dest: String, res: MessageFile)
case class PUTResultMsg(result: (StatusCode, String))
