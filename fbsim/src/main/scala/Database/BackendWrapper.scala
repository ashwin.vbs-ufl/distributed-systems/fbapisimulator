package fbAPI
import spray.json._
import MyJsonProtocol._
import spray.http.{StatusCodes, StatusCode}


abstract class DBWrapper{
	def exists(path: String): Boolean

	def isLeaf(path: String): Boolean
	def isPathNode(path: String): Boolean
	def createPathNode(path: String): Boolean
	def write(path: String, content: String): Boolean
	def read(path: String): String
	def erase(path: String): Boolean

	def AisFriendOfB(a: String, b: String): Boolean = {
		if(!isPathNode(b))
			return false
		if(!isLeaf(b + "/friendlist"))
			write(b + "/friendlist", Array(b).toJson.prettyPrint)
		val temp = read(b + "/friendlist").parseJson.convertTo[Array[String]]
		if(!temp.contains(a))
			return false
		return true
	}

	def popNode(path: String): (String, String) = {
		val pos = path.indexOf("/")
		if(pos == -1)
			return (path, "")
		else
			return (path.substring(0, pos), path.substring(pos + 1))
	}

	def get(path: String): Option[String] = {
		if(isLeaf(path))
			return Some(read(path))
		if(isPathNode(path))
			return Some("to be implemented")//getDirectory(parse.toString(), count)
		return None
	}

	def put(uid: String, destination: String, res: MessageFile): (StatusCode, String) = {
		var temp = popNode(destination)

		if(temp._1 == uid && popNode(temp._2)._2 == ""){
			if(write(destination, res.resourcePath))
				return (StatusCodes.OK, "put success")
			else
				return (StatusCodes.InternalServerError, "write failed")
		}

		while(temp._2 != ""){
			if(!AisFriendOfB(popNode(temp._2)._1, temp._1))
				return (StatusCodes.BadRequest, "friend chain broken")
			temp = popNode(temp._2)
		}
		if(uid != temp._1)
			return (StatusCodes.BadRequest, "dont have permission to write to this final directory")

		if(!isLeaf(res.resourcePath))
			return (StatusCodes.BadRequest, "resource file not found")

		if(write(destination + "/" + res.resourcePath.substring(1 + res.resourcePath.lastIndexOf("/")), res.toJson.compactPrint))
			return (StatusCodes.OK, "put success")
		else
			return (StatusCodes.InternalServerError, "write failed")
	}

	def post(uid: String, res: SignedFile): (StatusCode, String) = {

		val serializedFile = res.toJson.compactPrint

		var hash: String =  ""

		if(uid != ""){
			hash = MD5.hashString(serializedFile)
			write(uid + "/" + hash, serializedFile)
		}
		else{
			hash = MD5.hash(res.content.parseJson.convertTo[KeyFile].key).map("%02x".format(_)).mkString
			write("keyStore/" + hash, serializedFile)
			createPathNode(hash)
		}

		return (StatusCodes.OK, hash)
	}

	def delete(uid: String, res: String): Option[Boolean] = {
		if(exists(uid + "/" + res)){
			erase(uid + "/" + res)
			return Some(true)
		}
		else
			return None
	}

// 	def getDirectory(path: String, count: Int): Array[Resource] = {
// 		if(isPathNode(path))
// 			//TODO: return Resource Array
// 	}

	def setAttribute(uid: String, name: String, value: String): Boolean = {
		return write(uid + "/" + name, value)
	}

}
