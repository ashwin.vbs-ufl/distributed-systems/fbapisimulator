package fbAPI

import java.lang.String
import scala.collection.mutable.{Map, SynchronizedMap, HashMap}

object DSWrapper{
	class Node{
		var resources: Map[String, String] = {new HashMap[String, String] with SynchronizedMap[String, String]}
	}

	var nodes: Map[String, Node] = {new HashMap[String, Node] with SynchronizedMap[String, Node]}
}

class DSWrapper() extends DBWrapper{
	import DSWrapper._

// 	def getUID(path: String): String = {
// 		val pos = path.indexOf("/")
// 		if(pos == -1)
// 			return path
// 		else
// 			return path.substring(0, pos)
// 	}
//
// 	def getKey(path: String): String = {
// 		val pos = path.indexOf("/")
// 		if((pos == -1) || (pos == (path.length-1)))
// 			return ""
// 		else
// 			return path.substring(pos + 1)
// 	}

	def exists(path: String): Boolean = (isLeaf(path) || isPathNode(path))
	def isLeaf(path: String): Boolean = {
		path.replace("//", "/")
		var temp = popNode(path)
		return (nodes.contains(temp._1) && !(temp._2 == "") && nodes(temp._1).resources.contains(temp._2))
	}
	def isPathNode(path: String): Boolean = {
		path.replace("//", "/")
		var temp = popNode(path)
		return (nodes.contains(temp._1) && (temp._2 == ""))
	}

	def createPathNode(path: String): Boolean = {
		path.replace("//", "/")
		var temp = popNode(path)
		if(!(temp._2 == ""))
			return false
		nodes += (temp._1 -> (new Node))
		return true
	}

	def write(path: String, content: String): Boolean = {
		path.replace("//", "/")
		var temp = popNode(path)
		if(!nodes.contains(temp._1))
			return false
		if(temp._2 == "")
			return false
		nodes(temp._1).resources += (temp._2 -> content)
		return true
	}

	def read(path: String): String = {
		path.replace("//", "/")
		if(!isLeaf(path))
			return null
		var temp = popNode(path)
		return nodes(temp._1).resources(temp._2)
	}

	def erase(path: String): Boolean = {
		path.replace("//", "/")
		var temp = popNode(path)
		if(isLeaf(path)){
			nodes(temp._1).resources - temp._2
			return true
		}
		if(isPathNode(path)){
			nodes(temp._1).resources.clear()
			nodes -= temp._1
			return true
		}
		return false
	}
}
