package fbAPI

import java.nio.file.{Files, Paths, SimpleFileVisitor, FileVisitResult, Path, StandardOpenOption}
import java.io.File
// Access Types
// 0 read only user
// 1 owner
// 2 admin

class FSWrapper(rootPath: String) extends DBWrapper{
	var root: String = rootPath + "/"

	def exists(path: String): Boolean = Files.exists(Paths.get(root + path))
	def isLeaf(path: String): Boolean = exists(path) && !isPathNode(path)
	def isPathNode(path: String): Boolean = Files.isDirectory(Paths.get(root + path))

	def createPathNode(path: String): Boolean = {
		Files.createDirectories(Paths.get(root + path))
		return true
	}

	def write(path: String, content: String): Boolean = {
		Files.createDirectories(Paths.get(root + path.substring(0, path.lastIndexOf("/"))))

		Files.write(Paths.get(root + path), content.getBytes(), StandardOpenOption.CREATE_NEW, StandardOpenOption.TRUNCATE_EXISTING)
		return true
	}

	def read(path: String): String = {
		if(!isLeaf(path)){
			println(path)
			return ""
		}
		val byteArray = Files.readAllBytes(Paths.get(root + path))
		return new String(byteArray)
	}

	def erase(path: String): Boolean = {
		if(isLeaf(path))
			return Files.deleteIfExists(Paths.get(root + path))
		if(isPathNode(path)){
			def getRecursively(f: File): Seq[File] = f.listFiles.filter(_.isDirectory).flatMap(getRecursively) ++ f.listFiles
			getRecursively(new File(root + path)).foreach{f => f.delete()}
			return Files.deleteIfExists(Paths.get(root + path))
		}
		return false
	}
}
