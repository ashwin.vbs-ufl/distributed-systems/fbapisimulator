package fbAPI

import akka.actor.{Props, ActorRef, Actor}
import akka.pattern.ask
import akka.util.Timeout
import spray.can.Http
import spray.routing._
import spray.routing.directives._
import spray.routing.authentication.{ BasicAuth, UserPass }
import spray.http._
import spray.json._
import MyJsonProtocol._
import scala.concurrent.{Future, Await, ExecutionContext}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class ActConnection extends Actor with FERouteTrait{

	def actorRefFactory = context
	def receive = receiveAkkaMessages orElse runRoute(route)

	def receiveAkkaMessages: Receive = {
		case initConnection(canConnection: ActorRef, backEnd: ActorRef) => {
			backend = backEnd
			canConnection ! Http.Register(self)
// 			println("Connection Created")
		}

		case _:Http.ConnectionClosed => {
			context stop self
// 			println("Connection Closed")
		}
	}
}

trait FERouteTrait extends HttpService {

	var backend: ActorRef = null

	class connectionException

	def sane(path: String): Boolean = {
		if(path.contains(".") || path.contains("~") || path.contains("//"))
			return false
		return true
	}

	def checkTime(time: Long): Boolean = {
		val timestamp: Long = System.currentTimeMillis()
		return ((timestamp > time) && ((timestamp - time) < 5000))
	}

	implicit val timeout = Timeout(1.seconds)

	val route = {
		get {
			path(RestPath) { resPath => {
				if(!sane(resPath.toString))
					complete(StatusCodes.BadRequest, "trying to be a bad boy are we?")
				else{
					var future = backend ? GETRequestMsg(resPath.toString)
					val result = Await.result(future, timeout.duration).asInstanceOf[GETResultMsg]
					result.result match {
						case Some(name) =>
							complete(name)
						case None =>
							complete(StatusCodes.NotFound, "Resource Not Found")
					}
				}
			}
			}
		}~
		post {
			anyParams('content) { content => {
				val signedMessage = content.parseJson.convertTo[SignedMessage]
				if(signedMessage.uid == ""){
					val keyFile = signedMessage.content.parseJson.convertTo[KeyFile]
					if(!RSA.verifySignature(signedMessage.content.getBytes(), signedMessage.signature, keyFile.key) || !checkTime(keyFile.timestamp))
						complete(StatusCodes.BadRequest, "trying to be a bad boy are we?")
					else{
						val future = backend ? POSTRequestMsg(signedMessage.uid, SignedFile(signedMessage.content, signedMessage.signature))
						val result = Await.result(future, timeout.duration).asInstanceOf[POSTResultMsg]
						complete(result.result._1, result.result._2)
					}
				}
				else{
					var future = backend ? GETRequestMsg("keyStore/" + signedMessage.uid)
					val result = Await.result(future, timeout.duration).asInstanceOf[GETResultMsg]
					result.result match {
						case None =>
							complete(StatusCodes.NotFound, "PublicKey Not Found, Authentication Failed")
						case Some(key) => {
							val resourceFile = signedMessage.content.parseJson.convertTo[ResourceFile]
							if(!RSA.verifySignature(signedMessage.content.getBytes(), signedMessage.signature, key.parseJson.convertTo[SignedFile].content.parseJson.convertTo[KeyFile].key) || !checkTime(resourceFile.timestamp))
								complete(StatusCodes.BadRequest, "trying to be a bad boy are we?")
							else{
								val future = backend ? POSTRequestMsg(signedMessage.uid, SignedFile(signedMessage.content, signedMessage.signature))
								val result = Await.result(future, timeout.duration).asInstanceOf[POSTResultMsg]
								complete(result.result._1, result.result._2)
							}
						}
					}
				}
			}}
		}~
		delete {
			path(RestPath) { resPath => {
				anyParams('content) { content => {
					val signedMessage = content.parseJson.convertTo[SignedMessage]
					val deleteMessage = signedMessage.content.parseJson.convertTo[DeleteMessage]

					if(!sane(resPath.toString) || (deleteMessage.path != resPath.toString) || !checkTime(deleteMessage.timestamp))
						complete(StatusCodes.BadRequest, "trying to be a bad boy are we?")
					else{
						var future = backend ? GETRequestMsg("keyStore/" + signedMessage.uid)
						val result = Await.result(future, timeout.duration).asInstanceOf[GETResultMsg]
						result.result match {
							case None =>
								complete(StatusCodes.NotFound, "PublicKey Not Found, Authentication Failed")
							case Some(key) => {
								if(!RSA.verifySignature(signedMessage.content.getBytes(), signedMessage.signature, key.parseJson.convertTo[SignedFile].content.parseJson.convertTo[KeyFile].key))
									complete(StatusCodes.BadRequest, "signature invalid")
								else{
									var future = backend ? DELETERequestMsg(signedMessage.uid, resPath.toString)
									val result = Await.result(future, timeout.duration).asInstanceOf[DELETEResultMsg]
									result.result match {
										case Some(name) =>
											complete(name.toString())
										case None =>
											complete(StatusCodes.NotFound, "Resource Not Found")
									}
								}
							}
						}
					}
				}}
			}}
		}~
		put {
			path(RestPath) { resPath => {
				anyParams('content) { content => {
					val signedMessage = content.parseJson.convertTo[SignedMessage]
					val putResource = signedMessage.content.parseJson.convertTo[PutResource]

					if(!sane(resPath.toString) || (putResource.destination != resPath.toString) || !checkTime(putResource.timestamp))
						complete(StatusCodes.BadRequest, "trying to be a bad boy are we?")
					else{
						var future = backend ? GETRequestMsg("keyStore/" + signedMessage.uid)
						val result = Await.result(future, timeout.duration).asInstanceOf[GETResultMsg]
						result.result match {
							case None =>
								complete(StatusCodes.NotFound, "PublicKey Not Found, Authentication Failed")
							case Some(key) => {
								if(!RSA.verifySignature(signedMessage.content.getBytes(), signedMessage.signature, key.parseJson.convertTo[SignedFile].content.parseJson.convertTo[KeyFile].key))
									complete(StatusCodes.BadRequest, "signature invalid")
								else{
									var future = backend ? PUTRequestMsg(signedMessage.uid, putResource.destination, MessageFile(putResource.resourcePath, putResource.key, putResource.timestamp))
									val result = Await.result(future, timeout.duration).asInstanceOf[PUTResultMsg]
									complete(result.result._1, result.result._2)
								}
							}
						}
					}
				}}
			}}
		}
	}
}
