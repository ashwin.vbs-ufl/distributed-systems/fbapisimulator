package fbAPI

import akka.actor.{Props, ActorRef, Actor, Terminated}
import spray.can.Http
import akka.util.Timeout
import scala.concurrent.duration._

class ActListener extends Actor {
	implicit val timeout = Timeout(5.seconds)
	var be: ActorRef = null
	def receive = {
		case initListener(backend: ActorRef) => be = backend
		case _: Http.Connected => {
			val connection = context.actorOf(Props[ActConnection])
			connection ! initConnection(sender, be)
		}
		case _: Terminated => {
		}
	}
}
