package fbAPI

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.util.Timeout
import akka.routing.RoundRobinPool
import scala.concurrent.duration._
import spray.can.Http
import spray.json._
import MyJsonProtocol._

object Boot extends App {

	var count: Int = 128
	var path: String = "./repository/"

	// we need an ActorSystem to host our application in
	implicit val system = ActorSystem("fbAPI")

	// create and start our service actor
	val Listener = system.actorOf(Props[ActListener], "MainListener")
	val Backend = system.actorOf(RoundRobinPool(count).props(Props[ActBEInstance]), "BackEndRouter")
	for(i <- 1 to count)
		Backend ! initBEInstance(path)

	Listener ! initListener(Backend)

	implicit val timeout = Timeout(5.seconds)
	// start a new HTTP server on port 8080 with our service actor as the handler
	IO(Http) ! Http.Bind(Listener, interface = "localhost", port = 8088)
}
